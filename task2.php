<?php


function unparse_url($url)
{
    $parsed_url = parse_url($url);
    $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    $query    = isset($parsed_url['query']) ? $parsed_url['query'] : '';
    parse_str( $parsed_url['query'] , $query );
    asort($query);
    $sort_query = '';
    foreach ($query as $key=>$item){
        if($item == 3){
            unset($key, $item);
        } else{
            $sort_query .= $key.'='.$item. '&amp;';
        }
    }
    $result = $scheme.$host.'/?'.$sort_query.'url='.$path;
    if ($result == '/?url='){
        $result = '<br>';
    }
return $result;
}


echo unparse_url($_GET['url']);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>задание 3</title>
</head>

<body>
<form action="task2.php" method="get">
        <p><input style="width: 50vw" placeholder="https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3" type="text" name="url" /></p>
    <p><input type="submit" /></p>
</form>
</body>
</html>
