<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>итоговоое задание</title>
</head>

<body>
<form action="last_task.php" method="get">
    <p><input placeholder="id" type="text" name="id" /></p>
    <p><input type="submit" /></p>
</form>
</body>
</html>

<?php
echo get_data($_GET['id']);

function get_data($id){
    require 'vendor/autoload.php';
    $url = 'https://kit-consulting-dev.ru/test/orders/get?id='.$id;
    $obj = json_decode(file_get_contents($url))->result;
    $client = new \RetailCrm\ApiClient(
        'https://demo.retailcrm.ru',
        'T9DMPvuNt7FQJMszHUdG8Fkt6xHsqngH',
        \RetailCrm\ApiClient::V5
    );

    try {
        $response = $client->request->ordersCreate(array(
            'externalId' =>  $obj->{'email'},
            'firstName' => $obj->{'first_name'},
            'lastName' => $obj->{'last_name'},
            'items' => array(
                    $obj->{'items'}
            ),
            'delivery' => array(
                'code' => 'russian-post',
            )
        ));
    } catch (\RetailCrm\Exception\CurlException $e) {
        echo "Connection error: " . $e->getMessage();
    }

    if ($response->isSuccessful() && 201 === $response->getStatusCode()) {
        echo 'Order successfully created. Order ID into retailCRM = ' . $response->id;
        // or $response['id'];
        // or $response->getId();
    } else {
        echo sprintf(
            "Error: [HTTP-code %s] %s",
            $response->getStatusCode(),
            $response->getErrorMsg()
        );

        // error details
        //if (isset($response['errors'])) {
        //    print_r($response['errors']);
        //}
    }
}
?>
