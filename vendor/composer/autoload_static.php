<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite8abd1e1eab47d5c45d78e8e54a08fcd
{
    public static $prefixesPsr0 = array (
        'R' => 
        array (
            'RetailCrm\\' => 
            array (
                0 => __DIR__ . '/..' . '/retailcrm/api-client-php/lib',
            ),
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInite8abd1e1eab47d5c45d78e8e54a08fcd::$prefixesPsr0;
            $loader->classMap = ComposerStaticInite8abd1e1eab47d5c45d78e8e54a08fcd::$classMap;

        }, null, ClassLoader::class);
    }
}
